let video, mobileNet;
let label = '';

function modelResult(){
  console.log('Model is Ready !');
  mobileNet.predict(gotResults);
}

function gotResults(err, results){
  if (err) console.error(err);
  else {
    // console.log(results[0]);
    label = results[0].label
    mobileNet.predict(gotResults);
  }
}

function setup(){
  createCanvas(640, 550);
  video = createCapture(VIDEO);
  video.hide()
  background(0);
  mobileNet = ml5.imageClassifier('MobileNet', video, modelResult);
}

function draw (){
  background(0)
  image(video, 0, 0 )
  fill(255);
  textSize(32);
  text(label, 10, height - 20);
}